#!/usr/bin/env python3
import rospy
import random
from rsa_bravo_msgs.msg import BravoFloat, BravoMode


def talker():
    """
    Function to publish fake messages in the BravoFloat/BravoMode format.
    Used to test rqt_reach_monitor
    """
    rospy.init_node("test_talker", anonymous=True)
    r = rospy.Rate(5)

    names = ["voltage", "temperature", "position", "velocity", "current"]
    pubs = [
        rospy.Publisher(f"/bravo/bravo/{name}", BravoFloat, queue_size=10)
        for name in names
    ]
    pub_mode = rospy.Publisher("/bravo/bravo/mode", BravoMode, queue_size=10)
    msgs = [BravoFloat(val=random.uniform(0, 10)) for _ in range(len(names))]
    msg_mode = BravoMode()
    jnt_count = 7

    # Just so the mode doesn't change too quickly
    mode_options = [0, 1, 2, 3, 4]
    mode_weights = [50, 1, 5, 1, 1]

    while not rospy.is_shutdown():
        for msg, pub in zip(msgs, pubs):
            # Adjust all the values by +/- 10% randomly
            msg.val *= 1 + random.uniform(-0.1, 0.1)
            # Assign to a random joint
            msg.joint = random.randint(1, jnt_count)

            msg_mode.mode = random.choices(mode_options, mode_weights)[0]
            msg_mode.joint = msg.joint

            pub.publish(msg)
            pub_mode.publish(msg_mode)
        r.sleep()


if __name__ == "__main__":
    try:
        talker()
    except rospy.ROSInterruptException:
        pass
