# rqt_reach_monitor

This package is a [rqt](https://wiki.ros.org/rqt) [plugin](https://wiki.ros.org/rqt/Tutorials/Create%20your%20new%20rqt%20plugin) for displaying debug information about [Reach Robotics](https://reachrobotics.com/) manipulators.   It subscribes to custom messages defined in [rsa_bravo_msgs](https://gitlab.com/rsa-manipulation-testbed/rsa_bravo_msgs/).

Note this package **does not** actually talk to the manipulator.   It requires a driver (like [rsa_bravo_driver](https://gitlab.com/rsa-manipulation-testbed/rsa_bravo_driver)) to communicate with the arm and publish relevant messages.


## To use

* Clone this repo to the `src/` directory in a Catkin workspace
* Load related packages with `vcs import < rqt_reach_monitor/rqt_reach_monitor.repos`
* `catkin build`
* `source devel/setup.bash`
* Launch rqt

This plugin will appear in the "Plugins -> Robot Tools" menu as "Reach Monitor"


# Specification

Reach Robotics manipulators are built from sets of networked actuators.  Each actuator has a separate address/ID and each can report a set of standardized data values, for example the actuator position, velocity, and temperature.

The Bravo 7 arm has seven actators, numbered 1-7 with 1 being the gripper, 2 being the rotary "wrist", up to 7 being the rotator base;  ID 13 is the base computing module.

We're currently publishing two data types from each actuator:  the current ["mode"](https://gitlab.com/rsa-manipulation-testbed/rsa_bravo_msgs/-/blob/main/msg/BravoMode.msg?ref_type=heads) (disabled, standby, velocity control, etc) and [temperature](https://gitlab.com/rsa-manipulation-testbed/rsa_bravo_msgs/-/blob/main/msg/BravoTemperature.msg?ref_type=heads), but we'll probably add other messages in the future.

A [sample bagfile](assets/bravo_mode_temperature_sample_2023-11-09-12-50-00.bag) containing `BravoMode` and `BravoTemperature` messages is included in this repo.

Here's a mock-up for the plugin:

![mockup](figures/rqt_reach_mockup.png)

The data is displayed in a table, the first column is the joint index, then the mode, and the temperature.   As we define additional data values we can add columns.

Some details:

* The number of joints isn't fixed -- depending on which actuator your have there may be different numbers of joints.  The table should grow dynamically as it receives new joint numbers and should not expect joint numbers to be sequential.

* Thq plugin needs to provide a way to select which ROS topic(s) to display.   However, Bravo messages are published on multiple topics within a top-level namespace, for example, `/bravo/temperature` and `/bravo/mode` in the base namespace `/bravo/`.   The plugin needs to provide a drop-down which allows selection of the *base namespace*.  This is different from the way other rqt plugins work where you directly select the actual topic you want to view.

  One way to do this would be to look up all topics which publish `BravoMode` messages, then list the parent namespace of those messages.

# Execution
The current version of the UI looks like this:

![](figures/reach_ui_2.png)

It fulfills the above specification.

<sub><sup>Logo: Robot by wiran toni from Noun Project (CC BY 3.0)</sup></sub>
