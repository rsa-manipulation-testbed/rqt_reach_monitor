"""
RQT Reach Monitor plugin

Author: Anand Sekar
"""

import threading
from typing import List

import rospy
import rosservice
import rostopic
from python_qt_binding.QtCore import Qt, QTimer
from python_qt_binding.QtGui import QColor, QIcon
from python_qt_binding.QtWidgets import (
    QComboBox,
    QHBoxLayout,
    QHeaderView,
    QLabel,
    QPushButton,
    QSizePolicy,
    QTableWidget,
    QTableWidgetItem,
    QVBoxLayout,
    QWidget,
)
from qt_gui.plugin import Plugin

from rsa_bravo_msgs.msg import BravoFloat, BravoMode
from rsa_bravo_msgs.srv import EnableBravo


class ReachMonitor(Plugin):
    def __init__(self, context):
        super(ReachMonitor, self).__init__(context)
        self.setObjectName("ReachMonitor")

        self.mode_sub = None
        self.temp_sub = None
        self.position_sub = None
        self.velocity_sub = None
        self.current_sub = None
        self.voltage_sub = None

        self.setup_ui(context)

        self.update_timer = QTimer(self)
        self.update_timer.timeout.connect(self.timer_callback)
        self.update_timer.start(40)  # milliseconds

        # Whether the table needs to be reset due to choosing a new base namespace
        self.needs_reset = False

        # Keep track of whether there is a recently received message
        self.recent_mode_msg = False
        self.recent_temp_msg = False
        self.recent_position_msg = False
        self.recent_velocity_msg = False
        self.recent_current_msg = False
        self.recent_voltage_msg = False

        # Keep track of the current value of all messages
        self.current_mode = {}
        self.current_temp = {}
        self.current_position = {}
        self.current_velocity = {}
        self.current_current = {}
        self.current_voltage = {}

        # The mode messages
        self.mode_dict = {
            0: "Standby",
            1: "Disable",
            2: "Position",
            3: "Velocity",
            4: "Current",
        }

        # Blue, Red, Green, Orange, and Yellow (light)
        self.color_dict = {
            0: QColor(173, 216, 230),
            1: QColor(255, 200, 200),
            2: QColor(144, 238, 144),
            3: QColor(255, 218, 185),
            4: QColor(255, 255, 224),
        }

        self.joint_name_dict = {
            1: "J1 / A (118mm Jaw)",
            2: "J2 / B (Continuous Roll)",
            3: "J3 / C (180 Pitch)",
            4: "J4 / D (Continuous Roll)",
            5: "J5 / E (180 Pitch)",
            6: "J6 / F (180 Pitch)",
            7: "J7 / G (360 Yaw)",
        }

        # Lock the messages so they can't change underneath us while updating the table
        self.update_lock = threading.Lock()

    def save_settings(self, _plugin_settings, instance_settings):
        rospy.loginfo("Called save_settings.")
        base_ns = self.ns_combo_box.currentText()
        instance_settings.set_value("base_ns", base_ns)

    def restore_settings(self, _plugin_settings, instance_settings):
        rospy.loginfo("Called restore_settings.")
        base_ns = instance_settings.value("base_ns")

        self.ns_combo_box.addItem(base_ns)
        index = self.ns_combo_box.findText(base_ns)
        self.ns_combo_box.setCurrentIndex(index)
        self.base_ns_changed()

    def shutdown_plugin(self):
        # TODO: do we need to do anything here?
        pass

    def setup_ui(self, context):
        """
        Update the UI

        Args:
            context (_type_): _description_
        """
        self.widget = QWidget()

        # Overall layout
        self.layout = QVBoxLayout()

        # Row for selecting the namespace
        self.ns_hbox = QHBoxLayout()

        self.update_ns_push_button = QPushButton()
        self.update_ns_push_button.setIcon(QIcon.fromTheme("view-refresh"))
        self.update_ns_push_button.clicked.connect(self.update_ns)

        self.ns_combo_box = QComboBox()
        # Use `activated` rather than `currentIndexChanged`, because the latter
        # will also be triggered when we're programmatically modifying the namespace
        # list in response to a "Refresh" click.
        self.ns_combo_box.activated.connect(self.base_ns_changed)
        self.ns_combo_box.setSizeAdjustPolicy(QComboBox.AdjustToContents)
        self.ns_label = QLabel("Base namespace:")
        self.ns_hbox.addWidget(self.update_ns_push_button)
        self.ns_hbox.addWidget(self.ns_label)
        self.ns_hbox.addWidget(self.ns_combo_box)
        self.ns_hbox.addStretch(1)

        # Add disable/ enable buttons
        self.disable_button = QPushButton("Disable")
        self.disable_button.clicked.connect(self.disable)
        self.disable_button.setStyleSheet("QPushButton {color: red;}")
        self.ns_hbox.addWidget(self.disable_button)

        self.enable_button = QPushButton("Enable")
        self.enable_button.clicked.connect(self.enable)
        self.enable_button.setStyleSheet("QPushButton {color: green;}")
        self.ns_hbox.addWidget(self.enable_button)

        self.enable_service_name = None
        self.enable_bravo_proxy = None
        self.find_service()

        # Table for the monitor
        self.table_box = QHBoxLayout()
        self.table = QTableWidget()
        self.table.setRowCount(7)
        self.table.setColumnCount(7)
        self.table.setHorizontalHeaderLabels(
            [
                "Joint",
                "Mode",
                "Temperature (°C)",
                "Position (mm or radians)",
                "Velocity (mm/s or rad/s)",
                "Current (mA)",
                "Voltage (V)",
            ]
        )
        for i in range(self.table.columnCount()):
            self.table.horizontalHeader().setSectionResizeMode(i, QHeaderView.Stretch)
        self.table.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.table.verticalHeader().setVisible(False)
        self.table.horizontalHeader().setDefaultAlignment(Qt.AlignLeft)
        self.table.setEditTriggers(QTableWidget.NoEditTriggers)

        # Add boxes to layout and set it to widget
        self.table_box.addWidget(self.table)
        self.layout.addLayout(self.ns_hbox)
        self.layout.addLayout(self.table_box)
        self.widget.setLayout(self.layout)
        self.widget.setObjectName("Reach Monitor UI")
        # Show _widget.windowTitle on left-top of each plugin (when
        # it's set in _widget). This is useful when you open multiple
        # plugins at once. Also if you open multiple instances of your
        # plugin at once, these lines add number to make it easy to
        # tell from pane to pane.
        self.widget.setWindowTitle("Reach Monitor UI")
        if context.serial_number() > 1:
            self.widget.setWindowTitle(
                f"{self.widget.windowTitle()} ({context.serial_number()})"
            )

        # Add widget to the user interface
        context.add_widget(self.widget)

    def make_table_read_only(self):
        """
        Set the item flags to make the table read-only
        """
        for i in range(self.table.rowCount()):
            for j in range(self.table.columnCount()):
                item = self.table.item(i, j)
                item.setFlags(item.flags() & ~Qt.ItemIsEditable)

    def timer_callback(self):
        """
        Update Table when new mode/ float messages are available.
        """
        if self.needs_reset:
            self.table.clearContents()
            rospy.loginfo("Clearing table.")
            self.needs_reset = False

        with self.update_lock:
            if self.recent_mode_msg:
                self.update_table()
                self.recent_mode_msg = False
            if self.recent_temp_msg:
                self.update_table()
                self.recent_temp_msg = False
            if self.recent_position_msg:
                self.update_table()
                self.recent_position_msg = False
            if self.recent_velocity_msg:
                self.update_table()
                self.recent_velocity_msg = False
            if self.recent_current_msg:
                self.update_table()
                self.recent_current_msg = False
            if self.recent_voltage_msg:
                self.update_table()
                self.recent_voltage_msg = False

    def update_ns(self):
        """
        Update the list of namespaces which contain BravoMode messages
        """
        rospy.loginfo("Updating relevant namespaces.")

        # Save state so we can restore it if possible after updating namespaces
        selected_ns = self.ns_combo_box.currentText()
        rospy.loginfo(f"Initially selected namespaces: {selected_ns}")

        bravo_topics = self.get_topics("rsa_bravo_msgs/BravoMode", subscribed=False)
        bravo_namespaces = []
        for topic in bravo_topics:
            bravo_namespaces.append(topic[:-4])

        self.ns_combo_box.clear()

        for ns in bravo_namespaces:
            rospy.loginfo(f"Adding namespace {ns}.")
            self.ns_combo_box.addItem(ns)
            index = self.ns_combo_box.findText(selected_ns)
            rospy.loginfo(
                f"Index of previously-selected namespace ({selected_ns}): {index}"
            )
            if index == -1:
                self.ns_combo_box.addItem(selected_ns)
                index = self.ns_combo_box.findText(selected_ns)
            rospy.loginfo(f"Setting index to {index}.")
            self.ns_combo_box.setCurrentIndex(index)

    def disable(self):
        if self.enable_bravo_proxy is None:
            self.find_service()
        else:
            rospy.wait_for_service(self.enable_service_name)
            self.enable_bravo_proxy(0)
            rospy.loginfo("Disabling arm.")

    def enable(self):
        if self.enable_bravo_proxy is None:
            self.find_service()
        else:
            rospy.wait_for_service(self.enable_service_name)
            self.enable_bravo_proxy(1)
            rospy.loginfo("Enabling arm.")

    def find_service(self):
        services = rosservice.get_service_list()
        for s in services:
            if "enable_bravo" in s:
                self.enable_service_name = s
                break

        if self.enable_service_name:
            self.enable_bravo_proxy = rospy.ServiceProxy(
                self.enable_service_name, EnableBravo
            )
            rospy.loginfo(f"Service proxy created for '{self.enable_service_name}'")
        else:
            rospy.loginfo("Service 'enable_bravo' not found in any namespace.")

    def get_topics(self, message_type: str, subscribed: bool) -> List[str]:
        """
        List all topics in the rosgraph matching the input message type.
        """
        selected_topics = []
        pubs, subs = rostopic.get_topic_list()
        if subscribed:
            available_topics = subs
        else:
            available_topics = pubs

        for topic_name, topic_type, _ in available_topics:
            if topic_type == message_type:
                selected_topics.append(topic_name)
        return selected_topics

    def base_ns_changed(self):
        """
        Update subscriber to new topics.
        """

        if self.mode_sub is not None:
            self.mode_sub.unregister()
        if self.temp_sub is not None:
            self.temp_sub.unregister()
        if self.position_sub is not None:
            self.position_sub.unregister()
        if self.velocity_sub is not None:
            self.velocity_sub.unregister()
        if self.current_sub is not None:
            self.current_sub.unregister()
        if self.voltage_sub is not None:
            self.voltage_sub.unregister()

        mode_topic = self.ns_combo_box.currentText() + "mode"
        temp_topic = self.ns_combo_box.currentText() + "temperature"
        position_topic = self.ns_combo_box.currentText() + "position"
        velocity_topic = self.ns_combo_box.currentText() + "velocity"
        current_topic = self.ns_combo_box.currentText() + "current"
        voltage_topic = self.ns_combo_box.currentText() + "voltage"
        if any(
            topic == ""
            for topic in [
                mode_topic,
                temp_topic,
                position_topic,
                velocity_topic,
                current_topic,
                voltage_topic,
            ]
        ):
            return

        self.needs_reset = True

        rospy.loginfo(f"Changing mode and temp topics to {mode_topic}, {temp_topic}.")
        self.mode_sub = rospy.Subscriber(mode_topic, BravoMode, self.mode_callback)
        self.temp_sub = rospy.Subscriber(
            temp_topic, BravoFloat, self.temperature_callback
        )
        self.position_sub = rospy.Subscriber(
            position_topic, BravoFloat, self.position_callback
        )
        self.velocity_sub = rospy.Subscriber(
            velocity_topic, BravoFloat, self.velocity_callback
        )
        self.current_sub = rospy.Subscriber(
            current_topic, BravoFloat, self.current_callback
        )
        self.voltage_sub = rospy.Subscriber(
            voltage_topic, BravoFloat, self.voltage_callback
        )

    def mode_callback(self, msg: BravoMode):
        """
        Mode callback

        Args:
            msg (BravoMode): BravoMode message
        """
        with self.update_lock:
            try:
                self.current_mode[msg.joint] = msg.mode
                self.recent_mode_msg = True
            except KeyError:
                return

    def temperature_callback(self, msg: BravoFloat):
        """

        Temp Callback

        Args:
            msg (BravoFloat): BravoFloat message
        """
        with self.update_lock:
            try:
                self.current_temp[msg.joint] = msg.val
                self.recent_temp_msg = True
            except KeyError:
                return

    def position_callback(self, msg: BravoFloat):
        """
        Position Callback

        Args:
            msg (BravoFloat): BravoFloat message
        """
        with self.update_lock:
            try:
                self.current_position[msg.joint] = msg.val
                self.recent_position_msg = True
            except KeyError:
                return

    def velocity_callback(self, msg: BravoFloat):
        """
        Velocity Callback

        Args:
            msg (BravoFloat): BravoFloat message
        """
        with self.update_lock:
            try:
                self.current_velocity[msg.joint] = msg.val
                self.recent_velocity_msg = True
            except KeyError:
                return

    def current_callback(self, msg: BravoFloat):
        """
        Current Callback

        Args:
            msg (BravoFloat): BravoFloat message
        """
        with self.update_lock:
            try:
                self.current_current[msg.joint] = msg.val
                self.recent_current_msg = True
            except KeyError:
                return

    def voltage_callback(self, msg: BravoFloat):
        """
        Voltage Callback

        Args:
            msg (BravoFloat): BravoFloat message
        """
        with self.update_lock:
            try:
                self.current_voltage[msg.joint] = msg.val
                self.recent_voltage_msg = True
            except KeyError:
                return

    def display_joint(self, joint):
        if 0 <= joint <= 7:
            joint_text = self.joint_name_dict[joint]
        else:
            joint_text = str(joint)
        self.table.setItem(joint - 1, 0, QTableWidgetItem(joint_text))

    def update_table(self):
        """
        Update the table with current values
        """
        for joint, mode in self.current_mode.items():
            self.display_joint(joint)
            mode_item = QTableWidgetItem(self.mode_dict[mode])
            mode_item.setBackground(self.color_dict[mode])
            self.table.setItem(joint - 1, 1, mode_item)

        for joint, temp in self.current_temp.items():
            self.display_joint(joint)
            self.table.setItem(joint - 1, 2, QTableWidgetItem(f"{temp:.1f}"))

        for joint, pos in self.current_position.items():
            self.display_joint(joint)
            self.table.setItem(joint - 1, 3, QTableWidgetItem(f"{1000*pos:.1f}"))

        for joint, vel in self.current_velocity.items():
            self.display_joint(joint)
            self.table.setItem(joint - 1, 4, QTableWidgetItem(f"{vel:.1f}"))

        for joint, curr in self.current_current.items():
            self.display_joint(joint)
            self.table.setItem(joint - 1, 5, QTableWidgetItem(f"{curr:.1f}"))

        for joint, volt in self.current_voltage.items():
            self.display_joint(joint)
            self.table.setItem(joint - 1, 6, QTableWidgetItem(f"{volt:.1f}"))
